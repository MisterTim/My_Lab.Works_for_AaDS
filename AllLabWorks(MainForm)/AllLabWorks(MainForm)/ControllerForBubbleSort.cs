﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllLabWorks_MainForm_
{
    static class ControllerForBubbleSort
    {
        public static string[] BubbleSort(string[]mas)
        {
            for (int i = 0; i < mas.Length; i++)
                for (int j = 0; j < mas.Length - 1 - i; j++)
                    if (String.Compare(mas[j], mas[j + 1], false) > 0)
                        Swap(ref mas[j], ref mas[j + 1]);
            return mas; 
        }


        public static string[] ViborSort(string[] mas)
        {

            for (int i = 0; i < mas.Length - 1; i++)
            {
                int min = i;
                for (int j = i + 1; j < mas.Length; j++)
                {
                    if (String.Compare(mas[j], mas[min], false) < 0)
                    {
                        min = j;
                    }
                }
                Swap(ref mas[min], ref mas[i]);
            }
            return mas;
        }

        private static void Swap(ref string first, ref string second)
        {
            string factiveElem = first;
            first = second;
            second = factiveElem;
        }
    }
}
