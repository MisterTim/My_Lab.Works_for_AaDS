﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AllLabWorks_MainForm_
{
    public partial class BubbleSort : Form
    {
        public BubbleSort()
        {
            InitializeComponent();
        }

        private void StartSort_Click(object sender, EventArgs e)
        {
            string[] separators = { ",", ".", "!", "?", ";", ":", " " }; //Символы-разделители
            string[] mas = StartMassiv.Text.Trim().Split(separators, StringSplitOptions.RemoveEmptyEntries); //создаем массив из строк
            if (BubbleSortAcrive.Checked == true) //если выполняется пузырьковая сортировка
                ControllerForBubbleSort.BubbleSort(mas); //Сортируем!
            else //Иначе выполням сортировку выборками
                ControllerForBubbleSort.ViborSort(mas); //Сортируем!
            Result.Text = ""; //Очищаем текст окна с результатом сортировки
            for (int i = 0; i < mas.Length; i++) //И выводим в это окно результат новой сортировки!
                Result.Text += mas[i] + " "; 
        }

        private void StartMassiv_TextChanged(object sender, EventArgs e)
        {
            Result.Clear(); //Если меняем текст ввода, то текст окна с результатом очищается
        }
    }
}
