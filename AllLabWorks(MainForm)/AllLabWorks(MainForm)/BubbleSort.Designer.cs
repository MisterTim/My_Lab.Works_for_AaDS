﻿namespace AllLabWorks_MainForm_
{
    partial class BubbleSort
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.StartMassiv = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.BubbleSortAcrive = new System.Windows.Forms.RadioButton();
            this.ViberSortActive = new System.Windows.Forms.RadioButton();
            this.StartSort = new System.Windows.Forms.Button();
            this.Result = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // StartMassiv
            // 
            this.StartMassiv.Location = new System.Drawing.Point(12, 25);
            this.StartMassiv.Multiline = true;
            this.StartMassiv.Name = "StartMassiv";
            this.StartMassiv.Size = new System.Drawing.Size(308, 41);
            this.StartMassiv.TabIndex = 0;
            this.StartMassiv.TextChanged += new System.EventHandler(this.StartMassiv_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(302, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Запишите элементы, которые необходимо отсортировать";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ViberSortActive);
            this.groupBox1.Controls.Add(this.BubbleSortAcrive);
            this.groupBox1.Location = new System.Drawing.Point(15, 72);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(305, 77);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // BubbleSortAcrive
            // 
            this.BubbleSortAcrive.AutoSize = true;
            this.BubbleSortAcrive.Checked = true;
            this.BubbleSortAcrive.Location = new System.Drawing.Point(6, 19);
            this.BubbleSortAcrive.Name = "BubbleSortAcrive";
            this.BubbleSortAcrive.Size = new System.Drawing.Size(156, 17);
            this.BubbleSortAcrive.TabIndex = 0;
            this.BubbleSortAcrive.TabStop = true;
            this.BubbleSortAcrive.Text = "Пузырьковая сортировка";
            this.BubbleSortAcrive.UseVisualStyleBackColor = true;
            // 
            // ViberSortActive
            // 
            this.ViberSortActive.AutoSize = true;
            this.ViberSortActive.Location = new System.Drawing.Point(6, 42);
            this.ViberSortActive.Name = "ViberSortActive";
            this.ViberSortActive.Size = new System.Drawing.Size(174, 17);
            this.ViberSortActive.TabIndex = 1;
            this.ViberSortActive.Text = "Сортировка методом выбора";
            this.ViberSortActive.UseVisualStyleBackColor = true;
            // 
            // StartSort
            // 
            this.StartSort.Location = new System.Drawing.Point(12, 155);
            this.StartSort.Name = "StartSort";
            this.StartSort.Size = new System.Drawing.Size(308, 50);
            this.StartSort.TabIndex = 3;
            this.StartSort.Text = "Сортировать!";
            this.StartSort.UseVisualStyleBackColor = true;
            this.StartSort.Click += new System.EventHandler(this.StartSort_Click);
            // 
            // Result
            // 
            this.Result.Location = new System.Drawing.Point(12, 238);
            this.Result.Multiline = true;
            this.Result.Name = "Result";
            this.Result.ReadOnly = true;
            this.Result.Size = new System.Drawing.Size(308, 41);
            this.Result.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 222);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Результат:";
            // 
            // BubbleSort
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(332, 290);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Result);
            this.Controls.Add(this.StartSort);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.StartMassiv);
            this.Name = "BubbleSort";
            this.Text = "BubbleSort";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox StartMassiv;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton ViberSortActive;
        private System.Windows.Forms.RadioButton BubbleSortAcrive;
        private System.Windows.Forms.Button StartSort;
        private System.Windows.Forms.TextBox Result;
        private System.Windows.Forms.Label label2;
    }
}