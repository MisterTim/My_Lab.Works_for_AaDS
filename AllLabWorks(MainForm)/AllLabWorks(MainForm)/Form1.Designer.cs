﻿namespace AllLabWorks_MainForm_
{
    partial class AllLabWorks
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.RunBubbleSort = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // RunBubbleSort
            // 
            this.RunBubbleSort.Location = new System.Drawing.Point(12, 12);
            this.RunBubbleSort.Name = "RunBubbleSort";
            this.RunBubbleSort.Size = new System.Drawing.Size(332, 39);
            this.RunBubbleSort.TabIndex = 0;
            this.RunBubbleSort.Text = "Пузырьковая сортировка и сортировка методов вставок";
            this.RunBubbleSort.UseVisualStyleBackColor = true;
            this.RunBubbleSort.Click += new System.EventHandler(this.RunBubbleSort_Click);
            // 
            // AllLabWorks
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(356, 262);
            this.Controls.Add(this.RunBubbleSort);
            this.Name = "AllLabWorks";
            this.Text = "Выберите лабораторную работу";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button RunBubbleSort;
    }
}

